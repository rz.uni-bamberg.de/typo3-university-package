README
=================

gehört thematisch zu  https://typo3.org/teams-committees/academic-committee

Dieses Setup ist gedacht, um schnell und reproduzierbar eine typo3-Installation gemäß den Spezifikationen des Typo3-University-Packages bereitzustellen. 
Es ist getestet sowohl auf Notebooks als auch auf unseren Produktiv-Maschinen im Serverbetrieb. 

Innerhalb des Docker-Containers wird ein normaler ubuntu-server verwendet 

**Vorteile**

* bekannte Umgebung
* die Konfiguration kann bei Bedarf ohne Anpassungen auch ohne Docker verwendet werden 

Schritte um das System auf einem beliebigen Linux in Betrieb zu nehmen (müsste auch in virtualbox gehen):

## Aktueller Stand

* Grundlegendes Setup ist fertig
  * lauffähiges typo3 mit 
    * docker
    * t3kit

* schauen ob es prinzipiell funktioniert
* erste Basis für die weitere Entwicklung

### nächste Schritte

* ausgewählte Extensions hinzufügen
  * aus Liste UP
  * powermail
  * https://github.com/ElementareTeilchen/datatable
* Demo-Content hinzufügen
* Installation vereinfachen


## was ist drin

* typo3 
  * s. https://gitlab.com/rz.uni-bamberg.de/typo3-university-package/blob/master/docker/setup.sh

* Betriebssystem etc
  * https://gitlab.com/rz.uni-bamberg.de/typo3-university-package/blob/master/docker/Dockerfile

## Schritte um das System lokal zum laufen zu bekommen

### repo clonen
```
git clone git@gitlab.rz.uni-bamberg.de:itfl-service-public/typo3-university-package.git
```

### IP / Port anpassen / Prüfen
in Datei docker-compose.yml
sollte passen, wenn auf der Maschine sonst keine Server laufen

### pubkeys hinterlegen (optional)

Container startet ssh-server

Pubkey hinterlegen in Datei 
docker/authorized_keys2

### docker-compose installiert ?
aktuelle Version installieren 
wenn nicht vorhanden Anleitung z.B. für Ubuntu 16.04 
https://gitlab.rz.uni-bamberg.de/itfl-service-public/misc/raw/master/docker/install


### einschalten
```
#im Verzeichnis wo docker-compose.yml liegt
#einschalten
sudo docker-compose up -d

```

beim ersten Start werden die Kommandos aus 
docker/setup.sh
sugeführt



### typo3 Backend
wenn alles durchgelaufen ist, müsste nach einigen Sekunden ein fertiges typo3 im Browser erreichbar sein unter:

http://localhost/

(oder http://<IP:PORT>/  wie oben angegeben in docker-compose.yml

http://localhost/typo3
username:	root
password:	 rootroot


### einloggen in Container
```
ssh -p 220 root@localhost
oder wenn anders angegeben ssh -p [port aus docker-compose.yml] root@[ip aus docker-compose.yml]

alternativ docker-compose exec typo3-university-package bash
```


### container ausschalten 

```
#Der Container ist als autostart konfiguriert. Damit er nicht beim  Systemstart wieder angeht folgendes Kommando

sudo docker-compose down 

```

