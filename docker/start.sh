#!/bin/bash

#run once at very first startup of the container
sh /root/uniba.de/initial/remove-after-run/setup.sh
rm /root/uniba.de/initial/remove-after-run/setup.sh

chmod 700 /root/.ssh/authorized_keys2


/sbin/init

service ssh restart
service rsyslog restart
service apache2 restart
service mysql restart
cron
tail -f /start

